<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package savourschool
 */

get_header(); ?>
	<div class="front-page-wrapper">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="feature-section home-tab-section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="nav nav-pills nav-tabs nav-justified">
									<li><a data-toggle="tab" href="#home"><?php echo of_get_option('classes_title'); ?></a></li>
									<li class="active"><a data-toggle="tab" href="#online_classes"><?php echo of_get_option('online_classes_title'); ?></a></li>
									<li><a data-toggle="tab" href="#online_store"><?php echo of_get_option('online_store_title'); ?></a></li>
								</ul>
								<div class="tab-content">
									<div id="home" class="tab-pane fade" style="background-image: url('<?php echo of_get_option('classes_image'); ?>')">
										<div class="tab-content-inner">
											<?php echo of_get_option('classes_textarea'); ?>
											<a href="<?php echo of_get_option('classes_url'); ?>" class="home-tab-pills">View <?php echo of_get_option('classes_text_name'); ?> >></a>
										</div>
									</div>
									<div id="online_classes" class="tab-pane fade in active" style="background-image: url('<?php echo of_get_option('online_classes_image'); ?>')">
										<div class="tab-content-inner">
											<?php echo of_get_option('online_classes_textarea'); ?>
											<a href="<?php echo of_get_option('online_classes_url'); ?>" class="home-tab-pills">View <?php echo of_get_option('online_classes_text_name'); ?> >></a>
										</div>
									</div>
									<div id="online_store" class="tab-pane fade" style="background-image: url('<?php echo of_get_option('online_store_image'); ?>')">
										<div class="tab-content-inner">
											<?php echo of_get_option('online_store_textarea'); ?>
											<a href="<?php echo of_get_option('online_store_url'); ?>" class="home-tab-pills">View <?php echo of_get_option('online_store_text_name'); ?> >></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Newsletter -->
				<div class="newsletter-section" name="email_signup" id="email_signup">
					<div class="container">
						<div class="newsletter-wrapper">
							<div class="row">
								<div class="col-sm-12">
									<h2 class="title home-section-title"><?php echo of_get_option('newsletter_section_title'); ?></h2>
									<p class="newsletter-subtext">Sign up to the Savour Chocolate & Patisserie School newsletter for class <br />updates, discounts and the latest Savour School news</p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>
					</div>				
				</div>
				<!-- Newsletter End -->
				<!-- Shop -->
				<div class="home-product-section">
					<div class="shop-section">
						<div class="container">
							<?php // dynamic_sidebar( 'sidebar-product_category' ); ?>					
							<div class="row">
								<div class="col-sm-12">
									<h2 class="title home-section-title"><?php echo of_get_option('shop_section_title'); ?></h2>
									<p class="newsletter-subtext">We stock a wide range of quality chocolate and cake making supplies equipment and ingredients including <br /> Callebaut couverture chocolate imported from Belgium, chocolate moulds, silicon moulds and cake decorating supplies.</p>
								</div>
							</div>
						</div>
						<div class="container-fluid">
							<?php 
								$count = 0;
							    $shop_args = array(
							        'post_type' => 'product',
							        'posts_per_page'=> 2,
							        'meta_query' => array(
										array(
											'key'     => 'display_product_on_home',
											'value'   => 'yes',
											'compare' => 'LIKE',
										),
									),
							    );

						    // the query
						    $shop_query = new WP_Query( $shop_args ); ?>

							<?php if ( $shop_query->have_posts() ) : ?>
								<div class="row">
									<div class="product-feature-block">
										<!-- the loop -->
								        <?php while ( $shop_query->have_posts() ) : $shop_query->the_post(); ?>
								        	<?php $po_img = get_field( "product_large_image" ); 
								        	//get_post_meta( get_the_ID(), 'product_large_image', true); 
								        	?>
								        	<?php if ($count <= 0 ) { ?>
												<div class="col-sm-8 no-padding">
													<div class="home-product-inner" style="background-image: url('<?php echo $po_img; ?>')">
														<div class="home-product-meta">
															<a href="<?php the_permalink(); ?>" class="home-product-title"><?php echo get_the_title(); ?></a>
														</div>
													</div>
												</div>
											<?php } else { ?>
												<div class="col-sm-4 no-padding">
													<div class="home-product-inner" style="background-image: url('<?php echo $po_img; ?>')">
														<div class="home-product-meta">
															<a href="<?php the_permalink(); ?>" class="home-product-title"><?php echo get_the_title(); ?></a>
														</div>
													</div>
												</div>
											<?php } ?>
								        <?php 
									        $count++;
									        endwhile; 
								        ?>
								        <!-- end of the loop -->
								    	<!-- pagination here -->
									</div>
								</div>
					        <?php wp_reset_postdata(); ?>

						    <?php endif; ?>
						</div>	
					</div>
					<div class="whats-popular-section">
						<?php 

					    $product_args = array(
					        'post_type' => 'product',
					        'posts_per_page'=> 8,
					        'tax_query' => array(
					        		array(
										'taxonomy' => 'product_cat',
										'field' => 'slug',
										'terms' => 'popular'
									)
					        	)
					    );

					    // the query
					    $product_query = new WP_Query( $product_args ); ?>

					    <?php if ( $product_query->have_posts() ) : ?>

						<!-- pagination here -->
					    <div class="container">
							<div class="row">
								<div class="col-sm-12">
									<h2 class="title home-section-title"><?php echo of_get_option('whats_popular_section_title'); ?></h2>
								</div>
							</div>
						</div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-sm-12">
									<div class="product-grid">
										<div class="swiper-container home-product-wrapper">
											<div class="swiper-wrapper">
												<!-- the loop -->
										        <?php while ( $product_query->have_posts() ) : $product_query->the_post(); ?>
													<div class="swiper-slide">
										            <div class="grid-item">
										            	<div class="grid-inner">
										            		<img src="<?php echo the_post_thumbnail_url( 'isotope-thumb' ); ?>" class="img-responsive">
										            		<div class="popular-wrapper woocommerce">
									            				<h3 class="popular-title">
											            			<a href="<?php echo get_permalink(); ?>" class="">
											            				<?php echo get_the_title(); ?>
											            			</a>
									            				</h3>
										            			<div class="popular-excerpt">
										            				<?php echo get_the_excerpt(); ?>
										            			</div>
										            			<div class="popular-product-price">
										            				<?php
																			global $woocommerce, $product;
																			$currency = get_woocommerce_currency_symbol();
																			$price = get_post_meta( get_the_ID(), '_regular_price', true);
																			$sale = get_post_meta( get_the_ID(), '_sale_price', true);
																		?>
																		<?php if($sale) : ?>
																		<p class="popular-product-price-tickr"><del><?php echo $currency; echo round($price, 2); ?></del> <?php echo $currency; echo round($sale, 2); ?></p>    
																		<?php elseif($price) : ?>
																		<p class="popular-product-price-tickr"><?php echo $currency; echo round($price, 2); ?></p>    
																		<?php endif; ?>
										            			</div>
										            			<div class="popular-rating">
										            				<?php if ($average = $product->get_average_rating()) : ?>
																		   <?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																		<?php endif; ?>
										            			</div>
										            		</div>
										            	</div>
										            </div>
													</div>
										        <?php endwhile; ?>
										        <!-- end of the loop -->
											</div>
											<!-- Add Pagination -->
											<!-- <div class="swiper-pagination"></div> -->
											<!-- Add Arrows -->
											<div class="swiper-button-next"></div>
											<div class="swiper-button-prev"></div>
										</div>
									</div>
								</div>
							</div>

						        <!-- pagination here -->

						        <?php wp_reset_postdata(); ?>

						    <?php else : ?>
						        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						    <?php endif; ?>
						</div>
					</div>
				</div>
				<!-- Shop End -->
				<!-- Online Class -->
				<div class="home-online-class-section">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div id="online-class-title-wrapper">
									<h2 class="title home-section-title text-center"><?php echo of_get_option('latest_classes_section_title'); ?></h2>
								</div>
							</div>
						</div>
						<div class="row">
							<?php 
							$count_posts = wp_count_posts('online_classes');
							$published_posts = $count_posts->publish;

						    $online_classes_args = array(
						        'post_type' => 'online_classes',
						        'posts_per_page'=> 1,
						    );

						    // the query
						    $online_classes_query = new WP_Query( $online_classes_args ); 
						    $count = $online_classes_query->post_count; ?>

						    <?php if ( $online_classes_query->have_posts() ) : ?>

						        <!-- pagination here -->

						        <!-- the loop -->
						        <?php while ( $online_classes_query->have_posts() ) : $online_classes_query->the_post(); ?>
						            <div class="col-sm-8">
						            	<img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" class="online-class-img img-responsive">
						            </div>
						            <div class="col-sm-4">
							            <div class="online-class-meta">
							            	<div class="online-class-duration">
								            	<div class="row">
								            		<div class="col-sm-3">
									            		<div class="online-class-icon">
									            			<img src="<?php echo get_template_directory_uri().'/images/clock.png'; ?>" class="img-responsive">
									            		</div>								            			
								            		</div>
								            		<div class="col-sm-9">
									            		<div class="online-classes-text">
										            		<div class="online-classes-title">Cooking time</div>
											            	<div class="online-classes-desc"><h2><?php echo get_post_meta( get_the_ID(), 'duration', true); ?></h2></div>
									            		</div>								            			
								            		</div>
								            	</div>
							            	</div>
							            	<div class="online-class-difficulty">
							            		<div class="row">
													<div class="col-sm-3">
									            		<div class="online-class-icon">
									            			<img src="<?php echo get_template_directory_uri().'/images/speedometer.png'; ?>" class="img-responsive">
									            		</div>
													</div>
													<div class="col-sm-9">
									            		<div class="online-classes-text">
										            		<div class="online-classes-title">Difficulty</div>
												            <div class="online-classes-desc"><h2><?php $difficulty = get_post_meta( get_the_ID(), 'difficulty', true); 
												            $field = get_field_object('difficulty');
															$difficulty = $field['value'];
												             	if($difficulty){
													            	foreach ($difficulty as $value) {
																		echo $field['choices'][ $value ];
																	}
																}
												            ?></h2></div>
												            
												        </div>
													</div>
												</div>
							            	</div>
							            	<div class="online-class-people">
							            		<div class="row">
													<div class="col-sm-3">
									            		<div class="online-class-icon">
									            			<img src="<?php echo get_template_directory_uri().'/images/dishes.png'; ?>" class="img-responsive">
									            		</div>														
													</div>
													<div class="col-sm-9">
									            		<div class="online-classes-text">
										            		<div class="online-classes-title">Recommended for</div>
											            	<div class="online-classes-desc"><h2><?php $people = get_post_meta( get_the_ID(), 'people', true);
											            		echo $people.' People' ?></h2></div>
											           	</div>														
													</div>
												</div>
							            	</div>
							            </div>
							            <div class="total-video-section">
							            	<div class="row">
							            		<!--<div class="col-sm-4"></div>-->
							            		<div class="col-sm-12">
							            			<div class="online-post-count"><?php echo $published_posts; ?></div>
							            			<div><h5>VIDEO RECIPES</h5><p>…and growing!</p></div>
							            		</div>
							            	</div>
							            	<div class="row">
							            		<div class="col-sm-12 text-center">
							            			<a href="<?php echo get_permalink( get_page_by_path( 'online-classes' ) ); ?>" class="custom-black-btn">Learn more</a>
							            		</div>
							            	</div>
							            </div>							            
						            </div>
						        <?php endwhile; ?>
						        <!-- end of the loop -->

						        <!-- pagination here -->

						        <?php wp_reset_postdata(); ?>

						    <?php else : ?>
						        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						    <?php endif; ?>
						</div>
					</div>
				</div>
				<!-- Online Class End -->

				<!-- Hand On Classes -->
				<div class="upcoming-hand-on-classes-section">
					<div class="container">
						<?php dynamic_sidebar( 'sidebar-upcoming_class' ); ?>
						
						<?php			
						$atts = array(
							'title' => NULL,
							'limit' => 3,
							'css_class' => NULL,
							'show_expired' => FALSE,
							'month' => NULL,
							'category_slug' => NULL,
							'order_by' => 'start_date',
							'sort' => 'ASC'
						);
						// run the query
						global $wp_query; ?>
						<div class="upcomming-wrapper row">
							<?php 
								$wp_query = new EE_Event_List_Query( $atts );
								if (have_posts()) : while (have_posts()) : the_post();
							        ?>
							        <div class="col-sm-4 no-padding">
						        		<div class="upcomming-inner">
									        <a href="<?php echo get_permalink(); ?>">
									        	<img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" class="img-responsive upcomming-img">
										        <div class="upcomming-inner-text">
											        <div class="upcomming-meta-date">
											        	<?php $x = $post->EE_Event->first_datetime();  ?>
												        <div class="meta-month"><?php echo date("M", strtotime($x->start_date())); ?></div>
												        <div class="meta-day"><?php echo date("d", strtotime($x->start_date())); ?></div>
											        </div>
											        <div class="upcomming-meta-title">
											        	<div class="upcomming-title"><?php the_title(); ?></div>
											        </div>
										        </div>
									        </a>
						        		</div>
							        </div>   	
							<?php endwhile;  ?>
						</div>
						<?php else:
						?>
							<div>No Upcoming Events</div>
						<?php 
						    endif;
							// now reset the query and postdata
							wp_reset_query();
							wp_reset_postdata();
						?>
						<div class="row">
							<div class="col-sm-12">
								<div class="all-calender-links">
									<a href="<?php echo home_url().'/class-calendar/'; ?>" class="upcoming-all-calender-links">View Calendar</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Hand On Classes End -->
				
				<!-- Blog -->
				<div class="blog-section">
					<div class="container">
						<?php dynamic_sidebar( 'sidebar-popular_products' ); ?>	
						<?php 
						    $post_args = array(
						        'post_type' => 'post',
						        'posts_per_page'=> 3
						    );

						    // the query
						    $post_query = new WP_Query( $post_args ); ?>

						    <?php if ( $post_query->have_posts() ) : ?>

						        <!-- pagination here -->
						        <div class="row">
							        <div class="blog-head">
							        	<h1 class="blog-heading">Blog</h1>
							        </div>
							        <!-- the loop -->
							        <div class="blog-post">
								        <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
								        	<div class="col-sm-4 no-padding">
								        		<div class="blog-div">
								        			<a href="<?php echo get_permalink(); ?>">
											        	<img src="<?php echo the_post_thumbnail_url( 'full' ); ?>"/>
												        <div class="recent-post">
													        <div class="recent-post-date">
													        	<div class="post-month">
														        	<span><?php echo date("M", strtotime(get_the_date())); ?></span>
														        </div>
														        <div class="post-date">
														        	<span><?php echo date("d", strtotime(get_the_date())); ?></span>
														        </div>
													        </div>
													        <div class="recent-post-title">
													        	<span class="post-title"><?php the_title(); ?></span>
													        </div>
												        </div>
											        </a>
								        		</div>
								        	</div>
								        <?php endwhile; ?>
							        </div>
							        <div class="more-article">
									    <div class="col-sm-12">
									        
									        <div class="more">
										        <a href="<?php echo home_url('blog'); ?>">
										        	<button class="button-more-article">MORE ARTICLES</button>
										    	</a>
									        </div>
										    
									    </div>
									</div>
							        <!-- end of the loop -->
						        </div>

						        <!-- pagination here -->

						        <?php wp_reset_postdata(); ?>

						    <?php else : ?>
						        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						    <?php endif; ?>
					</div>
				</div>
				<!-- Blog End -->
				<div class="instagram-block">
					<div class="container">
						<div class="row">
							<?php dynamic_sidebar( 'sidebar-home_featured' ); ?>					
						</div>
					</div>
				</div>
			</main><!-- #main -->
		</div><!-- #primary -->
		
<?php get_footer(); ?>