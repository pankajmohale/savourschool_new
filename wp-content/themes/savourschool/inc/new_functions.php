<?php
/* New layout function */

/**
 * Enqueue scripts and styles.
 */
function new_layout_savourschool_scripts() {

	// New Layout File Enqueue - Style / Script
	wp_register_style( 'newlayout-style', get_template_directory_uri() .'/css/newlayout-style.css', array(), '21');
	wp_enqueue_style( 'newlayout-style' );
	
	wp_register_style( 'newlayout-media-style', get_template_directory_uri() .'/css/newlayout-media-style.css', array(), '21');
	wp_enqueue_style( 'newlayout-media-style' );

	wp_register_style( 'swiper-style', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css', array(), '21');
	wp_enqueue_style( 'swiper-style' );

	wp_register_script( 'swiper_js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js', array(), '1.0.0', true);
    	wp_enqueue_script( 'swiper_js' );

	wp_register_script( 'new_custom_js', get_template_directory_uri() . '/js/new_custom_js.js', array(), '1.0.0', true);
	wp_enqueue_script( 'new_custom_js' );
}
add_action( 'wp_enqueue_scripts', 'new_layout_savourschool_scripts' );

function new_layout_widgets_init() {

	register_sidebar( array(
        'name' => __( 'Footer Top Center', 'theme-slug' ),
        'id' => 'footer_top_center',
        'description' => __( 'this will add footer to the top section of main footer.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );

	register_sidebar( array(
        'name' => __( 'Footer Column One', 'theme-slug' ),
        'id' => 'footer_col_one',
        'description' => __( 'footer.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );

	register_sidebar(array(
        'name' => 'Footer Column Two',
        'id'   => 'footer_col_two',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Column Three',
        'id'   => 'footer_col_three',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Column Forth',
        'id'   => 'footer_col_forth',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Column Fifth',
        'id'   => 'footer_col_fifth',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Copyright',
        'id'   => 'footer_copyright',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
	
}
add_action( 'widgets_init', 'new_layout_widgets_init' );

function loadmorescript(){?>
<script type="text/javascript">
<?php global $wp_query; ?>
var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
var maxpages = "<?php  $wp_query->max_num_pages; ?>";
var page = 2;
jQuery(function($) {
    $('body').on('click', '.loadmore', function() {
        var button = $(this);
        var data = {
            'action': 'load_posts_by_ajax',
            'page': page,
            'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
        };
        $.ajax({
            url : ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                button.text('Loading...'); // change the button text, you can also add a preloader image
            },
            success : function( data ){
                if( data ) {
                    $('.blogLoopWrapper').append(data).slideDown( "slow", function() {
    // Animation complete.
  });
                    button.text( 'Load More' );
                    page++;
                    if ( page == maxpages ) 
                        button.remove();
                }else{
                    button.remove();
                }
            }
        });
    });
});
</script>
<?php }

add_action('wp_footer', 'loadmorescript');

function load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    $sticky = get_option( 'sticky_posts' );
    $args = array(
        'post__not_in'  => $sticky,
        'post_type' => 'post',
        'posts_per_page' => 6,
        'paged' => $paged,
        'post_status' => 'publish'
    );
    $my_posts = new WP_Query( $args );
    $i = 0;
    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
            <?php
                if($i % 3 == 0) { ?> 
                    <div class="row">
                <?php
                }
                    echo '<div class="bolgCol col-sm-4">';
                        echo '<div class="image"><a href="'.get_the_permalink($my_posts->post->ID).'">'.get_the_post_thumbnail( $my_posts->post->ID, 'full' ).'</a></div>';
                        echo '<div class="contentWrapper">';
                            echo '<div class="cat">'.get_the_category($my_posts->post->ID)[0]->name.'</div>';
                            echo '<h2><a href="'.get_the_permalink($my_posts->post->ID).'">'.get_the_title($my_posts->post->ID).'</a></h2>';
                            echo '<div class="desc">'.get_the_excerpt($my_posts->post->ID).'</div>';
                            echo '<div class="social_share">'.do_shortcode("[Sassy_Social_Share]").'</div>';
                        echo '</div>';
                    echo '</div>';
                
                    $i++; 
                  if($i != 0 && $i % 3 == 0) { ?>
                    </div><!--/.row-->
                    <div class="clearfix"></div>

                  <?php
                   } 
             ?>
        <?php endwhile ?>
        <?php
    endif;

    wp_die();
}

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');
