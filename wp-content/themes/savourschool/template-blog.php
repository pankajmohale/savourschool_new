<?php
/*
 Template Name: Blog Page Template
 */

get_header(); ?>

<div class="blogPostsContainer">
<?php $sticky = get_option( 'sticky_posts' );
	if ( !empty( $sticky ) ) {  // don't show anything if there are no sticky posts
	    $args = array(
	        'posts_per_page' => -1,  // show all sticky posts
	        'post__in'  => $sticky,
	        'ignore_sticky_posts' => 1,
	        'post_type' => 'post'
	    );
	    $query = new WP_Query( $args );
	    if ( $query->have_posts() ) {
	        $query->the_post(); ?>
	       <div class="feature_post">
	       	<div class="row">
	       		<div class="col-sm-7 imageWrap">
	       			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
	       		</div>
	       		<div class="content col-sm-5">
	       			<?php echo '<div class="cat">'.get_the_category($query->post->ID)[0]->name.'</div>'; ?>
	       			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	       			<div class="desc"><?php the_excerpt(); ?></div>
	       			<div class="social_share">
	       				<?php echo do_shortcode("[Sassy_Social_Share]"); ?>
	       			</div>
	       		</div>
	       	</div>
	       </div>
	      <?php wp_reset_postdata(); ?>
	    <?php }
	}
	 $args = array(
	    'posts_per_page' => -1,  // show all sticky posts
	    'post__not_in'  => $sticky,
	    'post_type' => 'post',
	    'posts_per_page' => 6,
	    'post_status' => 'publish'
	);
	// The Query
	$the_query = new WP_Query( $args );

	// The Loop
	$i = 0; 
	if ( $the_query->have_posts() ) {
		echo '<div class="blogLoopWrapper">';
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				if($i % 3 == 0) { 
					echo '<div class="row">';
				
				}
					echo '<div class="bolgCol col-sm-4">';
						echo '<div class="image"><a href="'.get_the_permalink().'">'.get_the_post_thumbnail( $the_query->post->ID, 'full' ).'</a></div>';
						echo '<div class="contentWrapper">';
							echo '<div class="cat">'.get_the_category($the_query->post->ID)[0]->name.'</div>';
							echo '<h2><a href="'.get_the_permalink().'">'.get_the_title().'</a></h2>';
							echo '<div class="desc">'.get_the_excerpt().'</div>';
							echo '<div class="social_share">'.do_shortcode("[Sassy_Social_Share]").'</div>';
						echo '</div>';
					echo '</div>';
				
					$i++; 
			       if($i != 0 && $i % 3 == 0) { 
				        echo '</div>';
				        echo '<div class="clearfix"></div>';
				   } 
			}
			
		echo '</div>';
		/* Restore original Post Data */
		wp_reset_postdata();
		if (  $the_query->max_num_pages > 1 ){
			echo '<div class="morepostsBtn"><div class="loadmore">Load More</div></div>'; // you can use <a> as well
		}
		
	} else {
		// no posts found
	}

?>
</div>
<?php get_footer();