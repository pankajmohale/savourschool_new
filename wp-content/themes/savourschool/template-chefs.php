<?php
/*
 Template Name: Chefs Template
 */


get_header(); ?>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

						// get_template_part( 'template-parts/content', 'page' );
						the_content();

					endwhile; // End of the loop.
					?>

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
	</div>
	<div class="row">
		<?php 

	    $chefs_args = array(
	        'post_type' => 'chef',
	        'posts_per_page'=> 2,
	        'orderby' => 'date',
			'order'   => 'ASC',
	        'meta_query' => array(
				array(
					'key'     => 'is_featured',
					'value'   => 'yes',
					'compare' => 'LIKE',
				),
			),
	    );

	    // the query
	    $chef_query = new WP_Query( $chefs_args ); ?>

	    <?php if ( $chef_query->have_posts() ) : ?>

	        <!-- pagination here -->
	        <div class="chefs-featured-wrapper">
		        <!-- the loop -->
		        <?php while ( $chef_query->have_posts() ) : $chef_query->the_post(); ?>
		            <div class="col-sm-6">
		            	<div class="featured-chefs">
		            		<div class="feature-img-meta">
				            	<img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" class="img-responsive feature-chefs" / >
		            		</div>
		            		<div class="feature-content-meta">
					            <h2><?php the_title(); ?></h2>
					            <p><?php echo get_the_excerpt(); ?></p>
					            <a href="<?php the_permalink(); ?>">learn more</a>
		            		</div>
		            	</div>
					</div>
		        <?php endwhile; ?>
		        <!-- end of the loop -->
	        </div>

	        <!-- pagination here -->

	        <?php wp_reset_postdata(); ?>

	    <?php else : ?>
	        <p><?php _e( 'Sorry, no featured chefs matched your criteria.' ); ?></p>
	    <?php endif; ?>
	</div>
	<div class="row">
		<div class="container">
			<div class="col-md-12">
				<div class="guest-chefs-title-wrapper">
					<h2><?php echo get_field( "guest_chefs_title" ); ?></h2>
					<?php echo get_field( "guest_chefs_description" ); ?>
				</div>
			</div>			
		</div>
	</div>
	<div class="guest-chefs-listing">

		<?php 
		$i = 1;

	    $chefslist_args = array(
	        'post_type' => 'chef',
	        'posts_per_page'=> -1,
	        'orderby' => 'date',
			'order'   => 'ASC',
	        'meta_query' => array(
				array(
					'key'     => 'is_featured',
					'value'   => 'yes',
					'compare' => 'NOT LIKE',
				),
			),
	    );

	    // the query
	    $chefslist_query = new WP_Query( $chefslist_args ); ?>

	    <?php if ( $chefslist_query->have_posts() ) : ?>

	    	<div class="row">
		        <!-- pagination here -->
		        <!-- the loop -->
		        <?php while ( $chefslist_query->have_posts() ) : $chefslist_query->the_post(); ?>
					<div class="col-md-4">
						<div class="guest-chefs-list-wrapper">
		            		<div class="guest-chefs-chefs">
			            		<div class="guest-chefs-img-meta">
					            	<img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" class="img-responsive guest-chefs-chefs" / >
			            		</div>
			            		<div class="guest-chefs-content-meta">
						            <h2><?php the_title(); ?></h2>
						            <p><?php echo get_the_excerpt(); ?></p>
						            <a href="<?php the_permalink(); ?>">View Class</a>
			            		</div>
			            	</div>
						</div>
					</div>			
					<?php 
						if($i % 3 == 0) {echo '</div><div class="row">';}
					?>
		        <?php $i++; endwhile; ?>
		        <!-- end of the loop -->
		    </div>

	        <!-- pagination here -->

	        <?php wp_reset_postdata(); ?>

	    <?php else : ?>
	        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	    <?php endif; ?>

	</div>		


<?php
get_footer();
