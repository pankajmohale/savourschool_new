<?php
/**
 * Template part for displaying Online Classes.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package savourschool
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="online-class-subscription-wrapper">
			
			<div class="online-subscription-section">
				<div class="container">
					
					<div class="row">
						<div class="col-md-12">
							<div class="online-subscription-header">
								<h1 class="subscription-head">You chocolate patisserie journey starts here</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
