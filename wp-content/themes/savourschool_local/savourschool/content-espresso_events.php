<?php
/**
 * This template will display a single event - copy it to your theme folder
 *
 * @ package		Event Espresso
 * @ author		Seth Shoultes
 * @ copyright	(c) 2008-2013 Event Espresso  All Rights Reserved.
 * @ license		http://eventespresso.com/support/terms-conditions/   * see Plugin Licensing *
 * @ link			http://www.eventespresso.com
 * @ version		4+
 */

/*************************** IMPORTANT *************************
 * if you are creating a custom template based on this file,
 * and do not wish to use the template display order controls in the admin,
 * then remove the following filter and position the additional template parts
 * that are loaded via the espresso_get_template_part() function to your liking
 * and/or use any of the template tags functions found in:
 * \wp-content\plugins\event-espresso-core\public\template_tags.php
 ************************** IMPORTANT **************************/
add_filter( 'FHEE__content_espresso_events__template_loaded', '__return_true' );

//echo '<br/><h6 style="color:#2EA2CC;">'. __FILE__ . ' &nbsp; <span style="font-weight:normal;color:#E76700"> Line #: ' . __LINE__ . '</span></h6>';

global $post;
$event_class = has_excerpt( $post->ID ) ? ' has-excerpt' : '';
$event_class = apply_filters( 'FHEE__content_espresso_events__event_class', $event_class );
?>
	<?php if(get_field('quick_text')): ?>
		<div class="container class_quick_test">
			<div class="row">
				<div class="col-md-12">
					<p><?php echo get_field('quick_text'); ?></p>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php 
			$galleryArray = get_post_gallery_ids($post->ID); 
			$thumb_count = 0;
			$main_count = 0;
		?>
		<?php if($galleryArray):?>		
		<div id="class-main-content" class="row">
			<div id="espresso-event-header-dv-<?php echo $post->ID;?>" class="espresso-event-header-dv">
				
	  		    <div id="slider">
		            <div id="carousel-bounding-box">
	                    <div id="myCarousel" class="carousel slide">
	                        <!-- main slider carousel items -->
	                        <div class="carousel-inner">
	                        	<?php foreach ($galleryArray as $id) { 
									$image_attributes = wp_get_attachment_image_src( $id, 'slider-img' );
	                        		?>
		                            <div class="item <?php if($main_count == 0){ echo 'active'; } ?>" data-slide-number="<?php echo $main_count; ?>">
		                                <img src="<?php echo $image_attributes[0]; ?>" class="img-responsive">
		                            </div>
		                            <?php $main_count++; ?>
	                            <?php } ?>
	                        </div>
	                   	</div>
	                </div>
				</div>
		    	<div id="slider-thumbs">
					<!--<ul class="list-inline">
						<?php //foreach ($galleryArray as $id) { ?>
							<li> 
								<a id="carousel-selector-<?php //echo $thumb_count; ?>" class="<?php //if($thumb_count == 0){ echo 'selected'; } ?>">
					            	<img src="<?php //echo wp_get_attachment_url( $id ); ?>" class="img-responsive">
					          	</a>
				          	</li>
				          	<?php //$thumb_count++; ?>
		      			<?php //} ?>
		          	</ul>-->
		          	 <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
		             <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>
			   	</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="container">
<?php do_action( 'AHEE_event_details_before_post', $post ); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class( $event_class ); ?>>
	<div class="row">
		<div class="col-md-3 hidden-xs class-categories">
			<div class="sidebar">
				<h2>Class Categories</h2>
				<?php $terms = get_terms( array( 
					    'taxonomy' => 'espresso_event_categories',
					    'parent'   => 0
					) );
					if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
					    echo '<ul>';
					    foreach ( $terms as $term ) {
					        echo '<li><a href="' . esc_url( get_term_link( $term ) ) . '">'. $term->name . '</a></li>';
					    }
					    echo '</ul>';
					}
				?>
			</div>
			<?php dynamic_sidebar('individual-class'); ?>
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
			<?php espresso_get_template_part( 'content', 'espresso_events-tickets' ); ?>
				<div class="espresso-event-wrapper-dv">
					<div class="class-features clearfix">
						<?php if (get_field('1st_feature_icon') && get_field('1st_feature_text')) { ?>
							<div class="feature-box">
								<p><img src="<?php echo get_field('1st_feature_icon'); ?>" alt="feature 1" /></p>
								<p><?php echo get_field('1st_feature_text'); ?></p>
							</div> 
						<?php } ?>
						<?php if (get_field('2nd_feature_icon') && get_field('2nd_feature_text')) { ?>
							<div class="feature-box">
								<p><img src="<?php echo get_field('2nd_feature_icon'); ?>" alt="feature 2" /></p>
								<p><?php echo get_field('2nd_feature_text'); ?></p>
							</div> 
						<?php } ?>
						<?php if (get_field('3rd_feature_icon') && get_field('3rd_feature_text')) { ?>
							<div class="feature-box">
								<p><img src="<?php echo get_field('3rd_feature_icon'); ?>" alt="feature 3" /></p>
								<p><?php echo get_field('3rd_feature_text'); ?></p>
							</div> 
						<?php } ?>
						<?php if (get_field('4th_feature_icon') && get_field('4th_feature_text')) { ?>
							<div class="feature-box">
								<p><img src="<?php echo get_field('4th_feature_icon'); ?>" alt="feature 4" /></p>
								<p><?php echo get_field('4th_feature_text'); ?></p>
							</div> 
						<?php } ?>
						<?php if (get_field('5th_feature_icon') && get_field('5th_feature_text')) { ?>
							<div class="feature-box">
								<p><img src="<?php echo get_field('5th_feature_icon'); ?>" alt="feature 5" /></p>
								<p><?php echo get_field('5th_feature_text'); ?></p>
							</div> 
						<?php } ?>
					</div>
					<?php if(get_field('class_video')){ ?>
					<div class="youtube-video">
						<?php echo get_field('class_video'); ?>
					</div>
					<?php } ?>
					<div id="exTab1">	
						<ul  class="nav nav-pills">
							<li class="active">
					        	<a  href="#1a" data-toggle="tab"><?php if(of_get_option('quick_guide_text')){echo of_get_option('quick_guide_text');}else{ echo 'Quick Guide'; } ?></a>
							</li>
							<?php if(get_field('what_you_will_learn')){ ?>
							<li>
								<a href="#2a" data-toggle="tab"><?php if(of_get_option('learning_text')){echo of_get_option('learning_text');}else{ echo 'Learning Outcomes'; } ?></a>
							</li>
							<?php } ?>
							<?php if(get_field('flavours')){ ?>
							<li>
								<a href="#3a" data-toggle="tab"><?php if(of_get_option('flavours_text')){echo of_get_option('flavours_text');}else{ echo 'Flavours'; } ?></a>
							</li>
							<?php } ?>
							<?php if(get_field('video')){ ?>
							<li>
								<a href="#4a" data-toggle="tab"><?php if(of_get_option('video_text')){echo of_get_option('video_text');}else{ echo 'Related Videos'; } ?></a>
							</li>
							<?php } ?>
							<?php if(get_field('faqs')){ ?>
							<li>
								<a href="#5a" data-toggle="tab"><?php if(of_get_option('faq_text')){echo of_get_option('faq_text');}else{ echo 'FAQ'; } ?></a>
							</li>
							<?php } ?>
<?php if(get_field('cancellation_policy')){ ?>
							<li>
								<a href="#6a" data-toggle="tab"><?php if(of_get_option('cancellation_policy_text')){echo of_get_option('cancellation_policy_text');}else{ echo 'Cancellation Policy'; } ?></a>
							</li>
<?php } ?>
							
						</ul>
						<div class="tab-content clearfix">
						  	<div class="tab-pane active" id="1a">
					          	<p><b>Level:</b></p>
					          	<p style="text-transform: capitalize;"><?php echo get_field('class_difficulty_level'); ?></p>
					          	<p></p>
					          	<p><b>Location:</b></p>
				          		<p><?php echo get_field('class_location'); ?></p>
					          	<p></p>
				          		<p><b>Class Details:</b></p>
					          	<?php espresso_get_template_part( 'content', 'espresso_events-details' ); ?>
							</div>
							<?php if(get_field('what_you_will_learn')){ ?>
							<div class="tab-pane" id="2a">
					          	<?php echo get_field('what_you_will_learn'); ?>
							</div>
							<?php } ?>
							<?php if(get_field('flavours')){ ?>
							<div class="tab-pane" id="3a">
					          	<?php echo get_field('flavours'); ?>
							</div>
							<?php } ?>
							<?php if(get_field('video')){ ?>
							<div class="tab-pane" id="4a">
					          	<div class="youtube-video">
									<?php echo get_field('video'); ?>
								</div>
							</div>
							<?php } ?>
							<?php if(get_field('faqs')){ ?>
							<div class="tab-pane" id="5a">
					          	<?php echo get_field('faqs'); ?>
							</div>
							<?php } ?>
<?php if(get_field('cancellation_policy')){ ?>
							<div class="tab-pane" id="6a">
								<?php echo get_field('cancellation_policy'); ?>
							</div>
							<?php } ?>
					  	</div>
					</div>
				</div>
			</div>
	</div>
	<div class="row">
		
		<div class="col-md-12" id="related-classes">
		<?php 
		
			$post_objects = get_field('related_classes');

			if( $post_objects ): ?>
				<h4>You may also like</h4>
			    <div class="row">
			    <?php foreach( $post_objects as $post_object): ?>
			        <div class="col-md-6 clearfix">
			        	<div class="class_image">
			        		<?php echo get_the_post_thumbnail($post_object->ID, array(200, 200)); ?>
			        	</div>
			        	<div class="class_details">
			        		<h3><?php echo get_the_title($post_object->ID); ?></h3>
			            	<a href="<?php echo get_permalink($post_object->ID); ?>" class="learn_more">Learn More</a>
			            </div>
			        </div>
			    <?php endforeach; ?>
			    </div>
			<?php endif; ?>
			<!--<footer class="event-meta">
				<?php //do_action( 'AHEE_event_details_footer_top', $post ); ?>
				<?php //do_action( 'AHEE_event_details_footer_bottom', $post ); ?>
			</footer>-->
		</div>
	</div>
</div>
<!-- #post -->
<?php do_action( 'AHEE_event_details_after_post', $post );

