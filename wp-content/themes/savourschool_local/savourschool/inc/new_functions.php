<?php
/* New layout function */

/**
 * Enqueue scripts and styles.
 */
function new_layout_savourschool_scripts() {

	// New Layout File Enqueue - Style / Script
	wp_register_style( 'newlayout-style', get_template_directory_uri() .'/css/newlayout-style.css', array(), '21');
	wp_enqueue_style( 'newlayout-style' );

	wp_register_style( 'swiper-style', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css', array(), '21');
	wp_enqueue_style( 'swiper-style' );

	wp_register_script( 'swiper_js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'swiper_js' );

	wp_register_script( 'new_custom_js', get_template_directory_uri() . '/js/new_custom_js.js', array(), '1.0.0', true);
	wp_enqueue_script( 'new_custom_js' );
}
add_action( 'wp_enqueue_scripts', 'new_layout_savourschool_scripts' );

function new_layout_widgets_init() {

	register_sidebar( array(
        'name' => __( 'Footer Top Center', 'theme-slug' ),
        'id' => 'footer_top_center',
        'description' => __( 'this will add footer to the top section of main footer.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );

	register_sidebar( array(
        'name' => __( 'Footer Column One', 'theme-slug' ),
        'id' => 'footer_col_one',
        'description' => __( 'footer.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );

	register_sidebar(array(
        'name' => 'Footer Column Two',
        'id'   => 'footer_col_two',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Column Three',
        'id'   => 'footer_col_three',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Column Forth',
        'id'   => 'footer_col_forth',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Column Fifth',
        'id'   => 'footer_col_fifth',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Copyright',
        'id'   => 'footer_copyright',
        'description'   => 'Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
	
}
add_action( 'widgets_init', 'new_layout_widgets_init' );