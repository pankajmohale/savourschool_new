<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package savourschool
 */

get_header(); ?>
	<div class="row">
		<?php if (get_field('banner_image')) : ?>
			<div class="chef-banner-img">
				<img src="<?php echo get_field('banner_image'); ?>" class="img-responsive"/>
			</div>
		<?php endif; ?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<div class="col-xs-12 col-sm-12 col-md-12">

				<?php
				while ( have_posts() ) : the_post(); ?>
					<div class="single-chef-content">
						<div class="single-chef-title">
							<h2><?php echo get_the_title(); ?></h2>
						</div>
						<div class="single-chef-designation">
							<h6><?php echo get_field('designation'); ?></h6>
						</div>
						<div class="single-chef-meta">
							<ul class="single-social">
								<?php if (get_field('facebook_links')) : ?>
									<li class="facebook-social">
										<a href="<?php echo get_field('facebook_links'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>	
									</li>
								<?php endif; ?>	 		
								<?php if (get_field('twitter_links')) : ?>
									<li class="twitter-social">
										<a href="<?php echo get_field('twitter_links'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>	
									</li>
								<?php endif; ?>	 		
								<?php if (get_field('twitter_links')) : ?>
									<li class="instagram-social">
										<a href="<?php echo get_field('instagram_links'); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>	
									</li>
								<?php endif; ?>	 		
								<?php if (get_field('twitter_links')) : ?>
									<li class="youtube-social">
										<a href="<?php echo get_field('google_links'); ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>	
									</li>
								<?php endif; ?>	 		
							</ul>
						</div>
						<div class="single-chef-info">
							<?php the_content(); ?>
						</div>					
					</div>

					<?php if(get_field('chefs_slider')): ?>
						<div class="single-chef-slider-wrapper">
							<div class="single-chef swiper-container">
								<div class="swiper-wrapper">
									<?php while(has_sub_field('chefs_slider')): ?>
										<div class="swiper-slide"><img src="<?php the_sub_field('slide'); ?>" class="img-responsive"/></div>
									<?php endwhile; ?>
								</div>
								<!-- Add Pagination -->
								<div class="swiper-pagination"></div>
								<!-- Add Arrows -->
								<div class="swiper-button-next"></div>
								<div class="swiper-button-prev"></div>
							</div>
						</div>
					<?php endif; ?>


				</div><!--col-md-8 col-xs-12 -->
				<?php if (get_field('quote_section')) :  ?>
					<div class="quote-content-wrapper">
						<div class="container">
							<div class="col-xs-12">
								<div class="quote-content"><?php echo get_field('quote_section'); ?></div>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<div class="col-xs-12">
					<?php
						// get_template_part( 'template-parts/content', 'post' );

						//the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
				</div>
				</main><!-- #main -->
			</div><!-- #primary -->
	</div>
<?php
//get_sidebar();
get_footer();
