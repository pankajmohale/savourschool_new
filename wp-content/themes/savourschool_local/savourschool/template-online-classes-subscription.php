<?php
/*
 Template Name: Online Class Subscription Template
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="custom-loader" class="custon-loader"></div>
		<main id="main" class="site-main" role="main">
			<div class="taxonomy-video-category-wrapper all-video">

				<div class="container">
					<div class="row">
						<div class="col-md-2">

						</div>
						<div class="col-md-8">
							<div class="taxonomy-video-text-section text-center all-video">
								<div class="page-header">
									<h1>You chocolate patisserie journey starts here</h1>
								</div><!-- .page-header -->

							</div>		
						</div>
						
						<div class="col-md-2">

						</div>
					</div>
				</div>
					
				<div class="all-video-section-wrapper all-video text-center">
					<div class="container">
						<div class="row">
							<div class="col-md-1">

							</div>
							<div class="col-md-10">
								<div class="row">
									<div class="taxonomy-video-section all-video text-center">
										<p class="taxonomy-para-text all-video">Latest videos</p>
									</div>
								    <?php $category = get_queried_object(); ?>

									<div id="ajax-all-video-post-list" class="">
								        <?php
								            $countargs = array(
							                    'post_type' => 'online_classes',
							                    'posts_per_page' => -1,		
							                    'meta_query' => array(
													array(
														'key'     => 'is_free_membership_video',
														'value'   => 'yes',
														'compare' => 'IN',
													),
												),					                    
								            );
								            $countloop = new WP_Query($countargs);

								            $postsPerPage = 12;
								            $args = array(
							                    'post_type' => 'online_classes',
							                    'posts_per_page' => $postsPerPage,
							                    'meta_query' => array(
													array(
														'key'     => 'is_free_membership_video',
														'value'   => 'yes',
														'compare' => 'IN',
													),
												),
							                    
								            );
								            $loop = new WP_Query($args);
								          ?>
										<?php if ( $loop->have_posts() ) : ?>

											<!-- pagination here -->

											<!-- the loop -->
											<?php while ($loop->have_posts()) : $loop->the_post(); ?>

								        	<?php get_template_part( 'template-parts/content', 'online_class_video' ); ?>
								        
								        	<?php endwhile; ?>
								         <?php wp_reset_postdata(); ?>

										<?php else : ?>
											<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
										<?php endif; ?>   

										<?php if ($countloop->post_count > 12) { ?>
											<div class="more-article clearfix">
										        <div class="col-sm-12">
											        <a id="load_more_online_posts" class="load_more_online_posts btn btn-primary more-video-custom-btn all-video" >More Videos</a>
											    </div>
											</div>
										<?php }	?>
								    </div>
																	
								</div>
							</div>
							<div class="col-md-1">

							</div>
						</div>
					</div>

				</div>
			</div>
			
		</main><!-- #main -->
	</div><!-- #primary -->
		
<?php
//get_sidebar();
get_footer();
