<?php
/**
 * Template part for displaying Online Classes.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package savourschool
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- <header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header --> 
	<div class="entry-content">
		<div class="online-class-container-wrapper">
			
			<div class="class-single-video-section">
				<div class="container">
					<div class="row">
						

						<div class="col-md-9 col-md-push-3">
							<!--<div class="online-class-filter">
								<div class="row">
									<div class="col-md-2 col-sm-2 hidden-xs">
										<div class="website_home text-center">
											<p><a href="<?php echo site_url(); ?>"><img class="header-img" src="<?php echo get_template_directory_uri(); ?>/images/home.png" /></a></p>
										</div>
									</div>
									<div class="col-md-5 col-sm-5">
										<div class="filter-first text-center">
											<p><a href="<?php echo site_url(); ?>/online-class-video/"><img class="header-img" src="<?php echo get_template_directory_uri(); ?>/images/video.png" /><span class="online-text">Latest Videos</span></a></p>
										</div>
									</div>
									<div class="col-md-5 col-sm-5">
										
										<div class="filter-second text-center">
											<div class="category-filter-wrapper text-right">
										    	<?php 
													$terms = get_terms( array(
													    'taxonomy' => 'video_category',
													    'hide_empty' => false,
													) );

													if ($terms) {									      
											    ?>
											    
												<div class="taxonomy-video-category_listing">
												    <div class="dropdown">
												    	<button class="btn btn-default dropdown-toggle taxonomy-video" type="button" data-toggle="dropdown"><img class="header-img" src="<?php echo get_template_directory_uri(); ?>/images/category.png" /><span class="online-text"> Search By Category</span><span class="caret"></span></button>
												    	<ul class="dropdown-menu">
												    		<?php 
												    		echo '<ul class="video-category-ul-list">';
 															foreach ( $terms as $term ) {
															 
															    // The $term is an object, so we don't need to specify the $taxonomy.
															    $term_link = get_term_link( $term );
															    
															    // If there was an error, continue to the next term.
															    if ( is_wp_error( $term_link ) ) {
															        continue;
															    }
															 
															    // We successfully got a link. Print it out.
															    echo '<li class="video-category-list"><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
															}
															 
															echo '</ul>';
												    		 ?>
												    		
												    	</ul>
												    </div>
										    	</div>
												<?php } ?>
									    	</div>
									    </div>											
								    	
									</div>
								</div>
							</div>-->
							<div class="online_classes_content_wrapper">
								<div class="text-center class_quick_test">
									<p><?php echo get_field('introductionary_text'); ?></p>
								</div>
								<div class="video-section">
									<?php 
									global $post;
									$thumb_count = 1;
									$main_count = 1;
									$galleryArray = miu_get_images($post->ID); ?>
									<div id="slider">
							            <div id="carousel-bounding-box">
						                    <div id="myCarousel" class="carousel slide">
						                        <!-- main slider carousel items -->
						                        <div class="carousel-inner">
						                        	<div class="item active" data-slide-number="0">
						                                <?php echo get_field('video'); ?>
						                            </div>
						                        	<?php foreach ($galleryArray as $id) { ?>
							                            <div class="item" data-slide-number="<?php echo $main_count; ?>">
							                                <img src="<?php echo $id; ?>" class="img-responsive">
							                            </div>
							                            <?php $main_count++; ?>
						                            <?php } ?>
						                        </div>
						                   	</div>
						                </div>
									</div>
									<?php if($galleryArray){ ?>
								    	<div id="slider-thumbs">
											<ul class="list-inline">
												<li>
													<a id="carousel-selector-0" class="selected">
										            	<img src="<?php echo of_get_option('online_video_thumbnail'); ?>" class="img-responsive">
										          	</a>	
												</li>
												
												<?php foreach ($galleryArray as $id) { ?>
													<li> 
														<a id="carousel-selector-<?php echo $thumb_count; ?>" class="">
											            	<img src="<?php echo $id; ?>" class="img-responsive">
											          	</a>
										          	</li>
										          	<?php $thumb_count++; ?>
								      			<?php } ?>
								          	</ul>
								          	<!-- <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
								             <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>-->
									   	</div>
								   	<?php } ?>
							   	</div>
								<div class="class-features clearfix">
									<?php if (get_field('1st_feature_icon') && get_field('1st_feature_text')) { ?>
										<div class="feature-box">
											<p><img src="<?php echo get_field('1st_feature_icon'); ?>" alt="feature 1" /></p>
											<p><?php echo get_field('1st_feature_text'); ?></p>
										</div> 
									<?php } ?>
									<?php if (get_field('2nd_feature_icon') && get_field('2nd_feature_text')) { ?>
										<div class="feature-box">
											<p><img src="<?php echo get_field('2nd_feature_icon'); ?>" alt="feature 2" /></p>
											<p><?php echo get_field('2nd_feature_text'); ?></p>
										</div> 
									<?php } ?>
									<?php if (get_field('3rd_feature_icon') && get_field('3rd_feature_text')) { ?>
										<div class="feature-box">
											<p><img src="<?php echo get_field('3rd_feature_icon'); ?>" alt="feature 3" /></p>
											<p><?php echo get_field('3rd_feature_text'); ?></p>
										</div> 
									<?php } ?>
									<?php if (get_field('4th_feature_icon') && get_field('4th_feature_text')) { ?>
										<div class="feature-box">
											<p><img src="<?php echo get_field('4th_feature_icon'); ?>" alt="feature 4" /></p>
											<p><?php echo get_field('4th_feature_text'); ?></p>
										</div> 
									<?php } ?>
									<?php if (get_field('5th_feature_icon') && get_field('5th_feature_text')) { ?>
										<div class="feature-box">
											<p><img src="<?php echo get_field('5th_feature_icon'); ?>" alt="feature 5" /></p>
											<p><?php echo get_field('5th_feature_text'); ?></p>
										</div> 
									<?php } ?>
								</div>

								<div class="download-recipe text-center">
									<a href="<?php the_field('pdf_link'); ?>" target="_blank" class="btn btn-primary download-receipe-btn">Download Recipe</a>
								</div>	
								<div class="online_tags">
									<?php $terms = get_the_terms ($post->id, 'video_tag');
									if ( !is_wp_error($terms)){ ?>
									<?php 
                                   	if($terms){
										$skills_links = wp_list_pluck($terms, 'name'); 
									    $skills_yo = implode(", ", $skills_links);
								    ?>
									<p>Tags: <?php echo $skills_yo; ?></p>
									<?php } } ?>
								</div>	
								<?php if(get_field('chef')){ ?>
									<div class="chef-section text-center">
										<div class="row">
											<div class="chef_info col-md-8">
												<?php $chef = get_field('chef'); ?>
												<p>RECIPE BY</p>
												<P><b><?php echo get_the_title($chef->ID); ?></b></p>
												<p>
													<?php echo get_post_field('post_content', $chef->ID); ?>
												</p>
											</div>	
											<div class="chef_photo col-md-4">
												<div><?php echo get_the_post_thumbnail($chef->ID); ?></div>
											</div>
										</div>	
									</div>
								<?php } ?>
								<?php if(get_field('tab_1') || get_field('tab_2') || get_field('tab_3') || get_field('tab_4') || get_field('tab_5')){ ?>
								<div id="exTab1">	
									<ul  class="nav nav-pills">
										<?php if(get_field('tab_1')){ ?>
										<li class="active">
								        	<a  href="#1a" data-toggle="tab"><?php if(of_get_option('1st_tab_text')){echo of_get_option('1st_tab_text');} ?></a>
										</li>
										<?php } ?>
										<?php if(get_field('tab_2')){ ?>
										<li>
											<a href="#2a" data-toggle="tab"><?php if(of_get_option('2nd_tab_text')){echo of_get_option('2nd_tab_text');} ?></a>
										</li>
										<?php } ?>
										<?php if(get_field('tab_3')){ ?>
										<li>
											<a href="#3a" data-toggle="tab"><?php if(of_get_option('3rd_tab_text')){echo of_get_option('3rd_tab_text');} ?></a>
										</li>
										<?php } ?>
										<?php if(get_field('tab_4')){ ?>
										<li>
											<a href="#4a" data-toggle="tab"><?php if(of_get_option('4th_tab_text')){echo of_get_option('4th_tab_text');}?></a>
										</li>
										<?php } ?>
										<?php if(get_field('tab_5')){ ?>
										<li>
											<a href="#5a" data-toggle="tab"><?php if(of_get_option('5th_tab_text')){echo of_get_option('5th_tab_text');} ?></a>
										</li>
										<?php } ?>
										
									</ul>
									<div class="tab-content clearfix">
										<?php if(get_field('tab_1')){ ?>
									  	<div class="tab-pane active" id="1a">
								          	<?php echo get_field('tab_1'); ?>
										</div>
										<?php } ?>
										<?php if(get_field('tab_2')){ ?>
										<div class="tab-pane" id="2a">
								          	<?php echo get_field('tab_2'); ?>
										</div>
										<?php } ?>
										<?php if(get_field('tab_3')){ ?>
										<div class="tab-pane" id="3a">
											<div class="youtube-video">
												<?php echo get_field('tab_3'); ?>
											</div>
										</div>
										<?php } ?>
										<?php if(get_field('tab_4')){ ?>
										<div class="tab-pane" id="4a">
								       		<?php echo get_field('tab_4'); ?>
										</div>
										<?php } ?>
										<?php if(get_field('tab_5')){ ?>
										<div class="tab-pane" id="5a">
								          	<?php echo get_field('tab_5'); ?>
										</div>
										<?php } ?>
								  	</div>
								</div>
								<?php } ?>
								<?php
									// If comments are open or we have at least one comment, load up the comment template.
									if ( comments_open() || get_comments_number() ) :
										comments_template();
									endif;
								?>
								
																
							</div>
						</div>
						<div class="col-md-3 col-md-pull-9">
							<?php dynamic_sidebar('individual-online-class'); ?>
						</div>
					</div>
				</div>
			</div>

		

		</div><!-- online-class-container-wrapper -->
	</div><!-- .entry-content -->
</article><!-- #post-## -->
